<?php

use App\Http\Controllers\Admin\ActionsController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\SpecializationsController;
use App\Http\Controllers\Admin\ServicesController;
use App\Http\Controllers\Admin\Specialists;
use App\Http\Controllers\Controller;
use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'language'], function () {
    Route::get('/', [Controller::class, 'main'])->name('main');
    Route::get('/specialists', [Controller::class, 'specialists'])->name('specialists');
    Route::get('/actions', [Controller::class, 'actions'])->name('actions');
    Route::get('/actions/{action}', [Controller::class, 'action'])->name('action');
    Route::get('/services', [Controller::class, 'services'])->name('services');
    Route::get('/services/{service}/{mode?}', [Controller::class, 'service'])->name('service');
    Route::get('/ajax/services/{specialization}', [Controller::class, 'ajaxSpecialization'])->name('services.ajax');

    Route::get('/contacts', [Controller::class, 'contacts'])->name('contacts');
    Route::post('/feedback', [Controller::class, 'feedback'])->name('feedback');
    Route::post('/record', [Controller::class, 'record'])->name('record');

    Route::get('/login', [LoginController::class, 'show'])->name('login');
    Route::post('/login', [LoginController::class, 'login'])->name('auth');
    Route::get('setlocale/{locale}', function($lang) {
        \Session::put('locale', $lang);

        return redirect()->back();
    })->name('setLocale');
});

Route::group(['middleware' => 'admin', 'prefix' => 'my-dashboard'], function() {
    Route::get('/', [AdminController::class, 'index'])->name('admin.index');
    Route::get('/orders/{type?}', [AdminController::class, 'orders'])->name('admin.orders');

    Route::get('/specialists', [Specialists::class, 'index'])->name('admin.specialists.index');
    Route::get('/specialists/delete/{doctor}', [Specialists::class, 'delete'])->name('admin.specialists.delete');
    Route::get('/specialists/create', [Specialists::class, 'create'])->name('admin.specialists.create');
    Route::post('/specialists/store', [Specialists::class, 'store'])->name('admin.specialists.store');
    Route::get('/specialists/edit/{doctor}', [Specialists::class, 'edit'])->name('admin.specialists.edit');
    Route::post('/specialists/update/{doctor}', [Specialists::class, 'update'])->name('admin.specialists.update');

    Route::get('/actions', [ActionsController::class, 'index'])->name('admin.actions.index');
    Route::get('/actions/create', [ActionsController::class, 'create'])->name('admin.actions.create');
    Route::get('/actions/delete/{action}', [ActionsController::class, 'delete'])->name('admin.actions.delete');
    Route::post('/actions/store', [ActionsController::class, 'store'])->name('admin.actions.store');
    Route::get('/actions/edit/{action}', [ActionsController::class, 'edit'])->name('admin.actions.edit');
    Route::post('/actions/update/{action}', [ActionsController::class, 'update'])->name('admin.actions.update');

    Route::get('/specializations', [SpecializationsController::class, 'index'])->name('admin.specializations.index');
    Route::get('/specializations/create', [SpecializationsController::class, 'create'])->name('admin.specializations.create');
    Route::get('/specializations/delete/{specialization}', [SpecializationsController::class, 'delete'])->name('admin.specializations.delete');
    Route::post('/specializations/store', [SpecializationsController::class, 'store'])->name('admin.specializations.store');
    Route::get('/specializations/edit/{specialization}', [SpecializationsController::class, 'edit'])->name('admin.specializations.edit');
    Route::post('/specializations/update/{specialization}', [SpecializationsController::class, 'update'])->name('admin.specializations.update');

    Route::get('/services', [ServicesController::class, 'index'])->name('admin.services.index');
    Route::get('/services/create', [ServicesController::class, 'create'])->name('admin.services.create');
    Route::get('/services/delete/{service}', [ServicesController::class, 'delete'])->name('admin.services.delete');
    Route::post('/services/store', [ServicesController::class, 'store'])->name('admin.services.store');
    Route::get('/services/edit/{service}', [ServicesController::class, 'edit'])->name('admin.services.edit');
    Route::post('/services/update/{service}', [ServicesController::class, 'update'])->name('admin.services.update');
});
