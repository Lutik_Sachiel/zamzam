<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LoginController
{
    public function show() {
        return view('login');
    }

    public function login(Request $request) {
        if ($request->input('key') === env('ADMIN_KEY')) {
            Session::put('admin', $request->input('key'));

            return redirect()->route('admin.index');
        }

        return redirect()->back();
    }
}
