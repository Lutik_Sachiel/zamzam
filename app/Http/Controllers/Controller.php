<?php

namespace App\Http\Controllers;

use App\Models\Action;
use App\Models\Doctor;
use App\Models\Order;
use App\Models\Services;
use App\Models\Specialization;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function specialists(Request $request) {
        $doctors = Doctor::query()->when($request->filled('specialization_id'), function ($builder) use ($request) {
            $builder->where('specialization_id', $request->query('specialization_id'));
        })->get();
        $specializations = Specialization::select(['id', 'name'])
            ->get()
            ->map(function ($specialization) use ($doctors) {
                $specialization->doctors = $doctors->where('specialization_id', $specialization->id)->all();

                return $specialization;
            });

        return view('specialists', [
            'specializations' => $specializations,
            'search'          => [
                'specialization' => (int) $request->query('specialization_id'),
            ],
        ]);
    }

    public function actions(Specialization $specialization) {
        return view('actions', [
            'specializations' => $specialization->getAllWithCache(),
            'actions' => Action::query()->considerLang()->orderBy('created_at', 'desc')->get()
        ]);
    }

    public function action(Action $action) {
        return view('action', [
            'action' => $action,
        ]);
    }

    public function contacts() {
        return view('contacts');
    }

    public function feedback(Request $request) {
        $to      = 'feedback@zamzam-med.kz';
        $message = 'Запрос звонка на номер ' . $request->input('phone');
        $headers = 'From: feedback@zamzam-med.kz' . "\r\n" .
            'Reply-To: feedback@zamzam-med.kz' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

        mail($to, 'Заказ консультации по телефону', $message, $headers);

        $order = new Order([
            'username' => '',
            'phone' => $request->input('phone'),
            'comment' => $message,
            'type' => 'feedback',
        ]);

        $order->save();

        return redirect()->back();
    }

    public function record(Request $request) {
        $to      = 'feedback@zamzam-med.kz';
        $message = 'Заявка от '.$request->input('username')
            . PHP_EOL . 'Телефон: '.$request->input('userphone')
            . PHP_EOL . 'Комментарий: '.$request->input('comment');
        $headers = 'From: feedback@zamzam-med.kz' . "\r\n" .
            'Reply-To: feedback@zamzam-med.kz' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

        mail($to, 'Запись на приём', $message, $headers);

        $order = new Order([
            'username' => $request->input('username'),
            'phone' => $request->input('userphone'),
            'comment' => $message,
            'type' => 'record',
        ]);

        $order->save();

        return redirect()->back();
    }

    public function main() {
        return view('welcome', [
            'actions' => Action::query()->orderBy('created_at', 'desc')->get(),
        ]);
    }

    public function services(Specialization $specialization) {
        return view('services', ['specializations' => $specialization->getAllWithCache()]);
    }

    public function ajaxSpecialization(Specialization $specialization, Services $services) {
        return $services->whereIn('id', $specialization->services ?: [])->get();
    }

    public function service(Services $service, $mode = 'description') {
        if ($mode === 'doctors') {
            $service['doctors'] = Doctor::whereIn('id', $service->doctors)->get();
        }

        return view('service', [
            'service' => $service,
            'mode' => $mode,
        ]);
    }
}
