<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreActionRequest;
use App\Http\Requests\UpdateActionRequest;
use App\Models\Action;

class ActionsController
{
    public function index() {
        $actions = Action::query()->orderBy('id', 'desc')->paginate(10);

        return view('admin.actions.index', [
            'actions' => $actions,
        ]);
    }

    public function create() {
        return view('admin.actions.create');
    }

    public function store(StoreActionRequest $request) {
        $pathPreview = $request->file('preview')->move('uploads/actions/', $request->file('preview')->getClientOriginalName());
        $pathCover = $request->file('cover')->move('uploads/actions/', $request->file('cover')->getClientOriginalName());

        $action = new Action([
            'name' => $request->input('name'),
            'text' => $request->input('text'),
            'lang' => $request->input('lang'),
            'preview' => '/'.$pathPreview,
            'cover' => '/'.$pathCover,
        ]);

        $action->save();
        return redirect()->route('admin.actions.index');
    }

    public function edit(Action $action) {
        return view('admin.actions.edit', [
            'action' => $action,
        ]);
    }

    public function update(Action $action, UpdateActionRequest $request) {
        $fields = [
            'name' => $request->input('name'),
            'text' => $request->input('text'),
            'lang' => $request->input('lang'),
        ];

        if ($request->hasFile('preview')) {
            $fields['preview'] = '/'.$request->file('preview')->move('uploads/actions', $request->file('preview')->getClientOriginalName());
        }

        if ($request->hasFile('cover')) {
            $fields['cover'] = '/'.$request->file('cover')->move('uploads/actions', $request->file('cover')->getClientOriginalName());
        }

        $action->update($fields);

        return redirect()->route('admin.actions.index');
    }

    public function delete(Action $action) {
        $action->delete();

        return redirect()->back();
    }
}
