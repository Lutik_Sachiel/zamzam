<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreSpecialistRequest;
use App\Http\Requests\UpdateSpecialistRequest;
use App\Models\Doctor;
use App\Models\Specialization;

class Specialists
{
    public function index() {
        $doctors = Doctor::query()->with('specialization')->latest()->paginate(10);

        return view('admin.specialists.index', [
            'doctors' => $doctors,
        ]);
    }

    public function create(Specialization $specialization) {
        return view('admin.specialists.create', [
            'specializations' => $specialization->getAllWithCache(),
        ]);
    }

    public function edit(Doctor $doctor, Specialization $specialization) {
        return view('admin.specialists.edit', [
            'doctor' => $doctor,
            'specializations' => $specialization->getAllWithCache(),
        ]);
    }

    public function store(StoreSpecialistRequest $request) {
        $path = $request->file('image')->move('uploads/doctors', $request->file('image')->getClientOriginalName());

        $doctor = new Doctor([
            'name' => $request->input('name'),
            'title' => $request->input('title'),
            'subtitle' => $request->input('subtitle'),
            'image' => '/'.$path,
            'specialization_id' => $request->input('specialization_id'),
        ]);

        $doctor->save();

        return redirect()->route('admin.specialists.index');
    }

    public function update(Doctor $doctor, UpdateSpecialistRequest $request) {
        $fields = [
            'name' => $request->input('name'),
            'title' => $request->input('title'),
            'subtitle' => $request->input('subtitle'),
            'specialization_id' => $request->input('specialization_id'),
        ];

        if ($request->hasFile('image')) {
            $fields['image'] = '/'.$request->file('image')->move('uploads/doctors', $request->file('image')->getClientOriginalName());
        }

        $doctor->update($fields);

        return redirect()->route('admin.specialists.index');
    }

    public function delete(Doctor $doctor) {
        $doctor->delete();

        return redirect()->back();
    }
}
