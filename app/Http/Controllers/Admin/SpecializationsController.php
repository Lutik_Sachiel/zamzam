<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SpecializationRequest;
use App\Http\Requests\StoreSpecialistRequest;
use App\Http\Requests\UpdateSpecialistRequest;
use App\Models\Doctor;
use App\Models\Services;
use App\Models\Specialization;

class SpecializationsController
{
    public function index() {
        $specializations = Specialization::query()->orderBy('id', 'desc')->paginate(8);

        return view('admin.specializations.index', [
            'specializations' => $specializations,
        ]);
    }

    public function create(Services $services) {
        return view('admin.specializations.create', [
            'services' => $services->getAllWithCache(),
        ]);
    }

    public function edit(Specialization $specialization, Services $services) {
        return view('admin.specializations.edit', [
            'services' => $services->getAllWithCache(),
            'specialization' => $specialization,
        ]);
    }

    public function store(SpecializationRequest $request) {
        $specialization = new Specialization($request->validated());

        $specialization->save();

        return redirect()->route('admin.specializations.index');
    }

    public function update(Specialization $specialization, SpecializationRequest $request) {
        $specialization->update($request->validated());

        return redirect()->route('admin.specializations.index');
    }

    public function delete(Specialization $specialization) {
        $specialization->delete();

        return redirect()->back();
    }
}
