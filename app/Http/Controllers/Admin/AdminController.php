<?php

namespace App\Http\Controllers\Admin;

use App\Models\Action;
use App\Models\Doctor;
use App\Models\Order;

class AdminController
{
    public function index() {
        $doctors = Doctor::query()->with('specialization')->inRandomOrder()->limit(5)->get();
        $doctorsCount = Doctor::query()->count();
        $actionsCount = Action::query()->count();

        return view('admin.index', [
            'doctors' => $doctors,
            'count' => [
                'doctors' => $doctorsCount,
                'actions' => $actionsCount,
                'records' => Order::where('type', 'record')->count(),
                'feedback' => Order::where('type', 'feedback')->count(),
            ]
        ]);
    }

    public function orders($type = 'record') {
        $orders = Order::where('type', $type)->orderBy('id', 'desc')->get();

        return view('admin.orders', [
           'orders' => $orders,
           'type' => $type === 'record' ? 'Записи на прием' : 'Заказы консультации по телефону',
        ]);
    }
}
