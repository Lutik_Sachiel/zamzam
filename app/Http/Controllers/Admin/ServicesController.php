<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreServiceRequest;
use App\Http\Requests\StoreSpecialistRequest;
use App\Http\Requests\UpdateSpecialistRequest;
use App\Models\Doctor;
use App\Models\Services;
use App\Models\Specialization;

class ServicesController
{
    public function index() {
        $services = Services::query()->paginate(8);

        return view('admin.services.index', [
            'services' => $services,
        ]);
    }

    public function create(Doctor $doctors) {
        return view('admin.services.create', [
            'doctors' => $doctors->getAllWithCache(),
        ]);
    }

    public function edit(Services $service, Doctor $doctors) {
        return view('admin.services.edit', [
            'doctors' => $doctors->getAllWithCache(),
            'service' => $service,
        ]);
    }

    public function store(StoreServiceRequest $request) {
        $data = $request->validated();
        $data['doctors'] = $request->input('doctors', []);
        $service = new Services($data);
        $service->save();

        return redirect()->route('admin.services.index');
    }

    public function update(Services $service, StoreServiceRequest $request) {
        $data = $request->validated();
        $data['doctors'] = $request->input('doctors', []);
        $service->update($data);

        return redirect()->route('admin.services.index');
    }

    public function delete(Services $service) {
        $service->delete();

        return redirect()->back();
    }
}
