<?php

namespace App\Http\Middleware;


use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        if (Session::get('admin') !== env('ADMIN_KEY'))
        {
            return redirect()->route('login');
        }

        return $next($request);
    }
}
