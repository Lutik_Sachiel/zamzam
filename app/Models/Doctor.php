<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    use AllCacheTrait;

    public $guarded = ['id'];

    public function specialization() {
        return $this->hasOne(Specialization::class, 'id', 'specialization_id');
    }
}
