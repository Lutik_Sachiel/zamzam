<?php

namespace App\Models;

use Illuminate\Support\Facades\Cache;

trait AllCacheTrait
{
    private $ttl = 60;

    public function getAllWithCache() {
        return Cache::remember(static::class . '__all', $this->ttl, static function () {
            return self::all();
        });
    }
}
