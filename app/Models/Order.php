<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

/**
 * Class Order
 * @package App\Models
 * @method select
 */

class Order extends Model
{
    protected $guarded = ['id'];
}
