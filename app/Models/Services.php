<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

/**
 * Class Services
 * @package App\Models
 * @method select
 */

class Services extends Model
{
    use AllCacheTrait;

    public $casts = [
        'doctors' => 'json',
    ];

    protected $guarded = ['id'];

    public function getTitleAttribute() {
        return $this->{'name_'.session('locale')};
    }

    public function getShortcutAttribute() {
        return $this->{'shortcut_'.session('locale')};
    }

    public function getDescriptionAttribute() {
        return $this->{'description_'.session('locale')};
    }
}
