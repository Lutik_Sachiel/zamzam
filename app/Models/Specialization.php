<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

/**
 * Class Specialization
 * @package App\Models
 * @method select
 */

class Specialization extends Model
{
    use AllCacheTrait;

    public $casts = [
        'services' => 'json',
    ];

    protected $guarded = ['id'];

    public function getTitleAttribute() {
        if (session('locale') === 'ru') {
            return $this->name;
        }


        return $this->{'name_'.session('locale')} ?: $this->name;
    }

    public function getDescriptionAttribute() {
        return $this->{'description_'.session('locale')};
    }
}
