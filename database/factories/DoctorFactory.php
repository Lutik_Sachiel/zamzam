<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Doctor;
use Faker\Generator as Faker;

$factory->define(Doctor::class, function (Faker $faker) {
    return [
        'name' => $this->faker->name,
        'title' => $faker->sentence(3),
        'subtitle' => $faker->sentence(8),
        'specialization_id' => 1,
        'image' => '/images/content/spec-5.svg'
    ];
});
