<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Action;
use Faker\Generator as Faker;

$factory->define(Action::class, function (Faker $faker) {
    return [
        'name' => $faker->words(2, true),
        'preview' => $faker->imageUrl(240, 117),
        'cover' => $faker->imageUrl(1600, 346),
        'text' => $faker->paragraph(4)
    ];
});
