<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertIntoSpecializations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Illuminate\Support\Facades\DB::table('specializations')->insert([
           [
               'name' => 'Амбулаторная хирургия',
           ],
           [
               'name' => 'Анализы, Рентген, УЗИ',
           ],
           [
               'name' => 'Ведение беременности и Роды',
           ],
           [
               'name' => 'Гастроэнтерология',
           ],
           [
               'name' => 'Гинекология',
           ],
           [
               'name' => 'Кардиология',
           ],
           [
               'name' => 'Комплексные обследования "ЧЕК-АПЫ"',
           ],
           [
               'name' => 'Маммология',
           ],
           [
               'name' => 'Неврология',
           ],
           [
               'name' => 'Неонатология',
           ],
           [
               'name' => 'Онкология',
           ],
           [
               'name' => 'Педиатрия',
           ],
           [
               'name' => 'Проктология',
           ],
           [
               'name' => 'Терапия (ВОП)',
           ],
           [
               'name' => 'Урология и Андрология',
           ],
           [
               'name' => 'Флебология',
           ],
           [
               'name' => 'Хирургия',
           ],
           [
               'name' => 'Эндокринология',
           ],
           [
               'name' => 'Другие услуги',
           ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Illuminate\Support\Facades\DB::table('specializations')->whereIn('name', [
            'Терапевт', 'Педиатр', 'Кардиолог', 'Хирург', 'Акушер-гинеколог',  'Уролог',
        ]);
    }
}
