@extends('_layout')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('main') }}">{{ __('app.layout.menu.main') }}</a></li>
            <li class="breadcrumb-item active">{{ __('app.layout.menu.services') }}</li>
        </ol>
    </nav>

    <div class="actions-page">
        <div class="actions-page__title">{{ __('app.layout.menu.services') }}</div>
        <div class="">
            <div class="actions-content">
                <div class="actions-content__title">{{ __('app.actions.subtitle') }}</div>
                <div class="actions-content__text">{!! __('app.actions.text') !!}</div>
                <div class="actions-content__list">
                    @foreach($specializations as $specialization)
                        <div class="actions-item">
                            <div class="actions-item__about">
                                <div class="actions-item__title">{{ $specialization->title }}</div>
                                <div class="actions-item__info d-none" id="info-specialization-{{ $specialization->id }}">
                                    <div class="actions-item__description">{{ $specialization->description }}</div>
                                    <div class="actions-item__services" id="services-specialization-{{ $specialization->id }}">

                                    </div>
                                </div>

                                <div class="actions-item__link js-show-specialization" id="show-specialization-{{ $specialization->id }}" data-specialization="{{ $specialization->id }}">
                                    {{ __('app.actions.btn') }}
                                </div>

                                <div class="actions-item__link js-hide-specialization d-none" id="hide-specialization-{{ $specialization->id }}" data-specialization="{{ $specialization->id }}">
                                    {{ __('app.actions.closeBtn') }}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
