@extends('_layout')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('main') }}">{{ __('app.layout.menu.main') }}</a></li>
            <li class="breadcrumb-item active">{{ __('app.layout.menu.actions') }}</li>
        </ol>
    </nav>

    <div class="actions-page">
        <div class="actions-page__title">{{ __('app.layout.menu.actions') }}</div>
        <div class="">
            <div class="actions-content">
                <div class="actions-content__title">{{ __('app.actions.subtitle') }}</div>
                <div class="actions-content__text">{!! __('app.actions.text') !!}</div>
                <div class="actions-content__list">
                    @foreach($actions as $action)
                        <div class="actions-item">
                            <div class="actions-item__about">
                                <div class="actions-item__title">{{ $action->name }}</div>
                                <div class="actions-item__link"><a href="{{ route('action', ['action' => $action->id]) }}">{{ __('app.actions.btn') }}</a></div>
                            </div>
                            <div class="actions-item__image">
                                <img src="{{ $action->preview }}" alt=""/>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
