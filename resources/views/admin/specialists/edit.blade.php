@extends('admin._layout')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Редактировать специалиста
                            </h2>
                        </div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="body" method="post" action="{{ route('admin.specialists.update', ['doctor' => $doctor->id]) }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="name" class="form-control" placeholder="Имя" value="{{$doctor->name}}" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="title" class="form-control" placeholder="Надпись"  value="{{$doctor->title}}" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="subtitle" class="form-control" placeholder="Вторая надпись" value="{{$doctor->subtitle}}" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div style="max-width: 100%"><img src="{{ $doctor->image }}"/></div>
                                        <div class="form-line">
                                            <div>Картинка</div>
                                            <input type="file" name="image" class="form-control" placeholder="Картинка">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <div>Специализация</div>
                                            <select name="specialization_id" required  class="form-control">
                                                @foreach ($specializations as $specialization)
                                                    <option value="{{ $specialization->id }}" @if ((int) $doctor->specialization_id === (int) $specialization->id) selected @endif>
                                                        {{ $specialization->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary">Сохранить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
