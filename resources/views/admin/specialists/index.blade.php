@extends('admin._layout')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Специалисты</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="{{ route('admin.specialists.create') }}" class="dropdown-toggle">
                                        <i class="material-icons">add</i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Специалист</th>
                                        <th>Специализация</th>
                                        <th>Картинка</th>
                                        <th>Действия</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($doctors as $doctor)
                                            <tr>
                                                <td>{{ $doctor->id }}</td>
                                                <td>{{ $doctor->name }}</td>
                                                <td>{{ $doctor->specialization->name }}</td>
                                                <td>
                                                    <a href="{{ $doctor->image }}" target="_blank">Открыть</a>
                                                </td>
                                                <td>
                                                    <a href="{{ route('admin.specialists.edit', ['doctor' => $doctor->id]) }}" class="btn btn-default"><i class="material-icons">edit</i> Редактирование</a>

                                                    <a href="{{ route('admin.specialists.delete', ['doctor' => $doctor->id]) }}" class="btn btn-default">
                                                        <i class="material-icons">delete</i> Удалить
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $doctors->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
