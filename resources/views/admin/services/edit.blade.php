@extends('admin._layout')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Редактировать услугу
                            </h2>
                        </div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="body" method="post" action="{{ route('admin.services.update', ['service' => $service->id]) }}">
                            {{ csrf_field() }}
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="name_ru" class="form-control" value="{{ $service->name_ru }}" placeholder="Название (RU)" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="name_kk" class="form-control" value="{{ $service->name_kk }}" placeholder="Название (KK)" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea class="form-control" name="shortcut_ru" placeholder="Короткое описание (RU)" required>{{ $service->shortcut_ru }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea class="form-control" name="shortcut_kk" placeholder="Короткое описание (KK)" required>{{ $service->shortcut_kk }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea class="form-control" name="description_ru" placeholder="Описание (RU)" required>{{ $service->description_ru }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea class="form-control" name="description_kk" placeholder="Описание (KK)" required>{{ $service->description_kk }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <div>Врачи</div>
                                            <select name="doctors[]" multiple class="form-control">
                                                @foreach($doctors as $doctor)
                                                    <option value="{{ $doctor->id }}" @if (in_array($doctor->id, $service->doctors)) selected @endif>{{ $doctor->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary">Сохранить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
