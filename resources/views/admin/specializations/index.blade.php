@extends('admin._layout')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Специализации</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="{{ route('admin.specializations.create') }}" class="dropdown-toggle">
                                        <i class="material-icons">add</i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Специализация</th>
                                        <th>Описание</th>
                                        <th>Действия</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($specializations as $specialization)
                                            <tr>
                                                <td>{{ $specialization->id }}</td>
                                                <td>{{ $specialization->name }}</td>
                                                <td>{{ $specialization->description_ru ?? $specialization->description_kk }}</td>
                                                <td>
                                                    <a href="{{ route('admin.specializations.edit', ['specialization' => $specialization->id]) }}" class="btn btn-default"><i class="material-icons">edit</i> Редактирование</a>

                                                    <a href="{{ route('admin.specializations.delete', ['specialization' => $specialization->id]) }}" class="btn btn-default">
                                                        <i class="material-icons">delete</i> Удалить
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $specializations->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
