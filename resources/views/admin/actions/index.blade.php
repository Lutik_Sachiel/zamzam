@extends('admin._layout')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Акции</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="{{ route('admin.actions.create') }}" class="dropdown-toggle">
                                        <i class="material-icons">add</i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Название</th>
                                        <th>Текст</th>
                                        <th>Язык</th>
                                        <th>Картинка</th>
                                        <th>Действия</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($actions as $action)
                                            <tr>
                                                <td>{{ $action->id }}</td>
                                                <td>{{ $action->name }}</td>
                                                <td>{{ \Illuminate\Support\Str::limit($action->text, 100) }}</td>
                                                <td>{{ $action->lang }}</td>
                                                <td>
                                                    <a href="{{ $action->preview }}" target="_blank">Открыть</a>
                                                </td>
                                                <td>
                                                    <a href="{{ route('admin.actions.edit', ['action' => $action->id]) }}" class="btn btn-default">
                                                        <i class="material-icons">edit</i> Редактирование
                                                    </a>
                                                    <a href="{{ route('admin.actions.delete', ['action' => $action->id]) }}" class="btn btn-default">
                                                        <i class="material-icons">delete</i> Удалить
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $actions->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
