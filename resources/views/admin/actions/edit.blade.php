@extends('admin._layout')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Редактировать
                            </h2>
                        </div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="body" method="post" action="{{ route('admin.actions.update', ['action' => $action->id]) }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="name" class="form-control" placeholder="Название" value="{{$action->name}}" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea name="text" class="form-control" placeholder="Надпись" required>{{$action->text}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div ><img src="{{ $action->preview }}" style="max-width: 100%"/></div>
                                        <div class="form-line">
                                            <div>Картинка (mini)</div>
                                            <input type="file" name="preview" class="form-control" placeholder="Картинка">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div ><img style="max-width: 70%" src="{{ $action->cover }}"/></div>
                                        <div class="form-line">
                                            <div>Картинка (max)</div>
                                            <input type="file" name="cover" class="form-control" placeholder="Картинка">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <div>Язык</div>
                                            <select name="lang" required class="form-control">
                                                <option value="ru">RU</option>
                                                <option value="kk" @if ($action->lang === 'kk') selected @endif>KK</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary">Сохранить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
