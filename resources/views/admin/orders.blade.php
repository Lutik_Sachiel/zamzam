@extends('admin._layout')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>{{ $type }}</h2>
            </div>
            <div class="row clearfix">
                @foreach($orders as $order)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                {{ $order->created_at }}
                            </h2>
                        </div>
                        <div class="body">
                            {!! nl2br($order->comment) !!}
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
