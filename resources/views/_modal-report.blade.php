<div class="modal-component__title">Онлайн заявка</div>
<div class="modal-component__subtitle">Заполните форму и мы подберем для Вас удобное время и условия!</div>
<form class="modal-component__form" method="post" action="{{ route('record') }}" id="request-form">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-6 col-md-6">
            <input type="text" placeholder="Введите ваше имя" class="modal-component__input" name="username" required/>
        </div>
        <div class="col-6 col-md-6">
            <input type="text" placeholder="Введите ваш номер: +7 (- - -) - - -  - -  - -" class="modal-component__input" name="userphone" required/>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <textarea placeholder="Введите ваш комментарий" class="modal-component__textarea" name="comment"></textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9 col-8 modal-component__hint">
            Заполняя данную форму, Вы соглашаетесь c условиями обработки персональных данных. Мы гарантируем конфиденциальность Вашего обращения!
        </div>
        <div class="col-md-3 col-4">
            <button class="modal-component__button g-recaptcha"
                    data-sitekey="6LfHMlcaAAAAAG7t04y5o30DYh98CbSKhpknpbUn"
                    data-callback='requestForm'
                    data-action='submit'>Записаться к врачу</button>
        </div>
    </div>
</form>
