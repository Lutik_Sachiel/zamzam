@extends('_layout')
@section('content')
    <section>
        <div class="main-banner">
            <div class="main-banner__wrapper">
                <div class="main-banner__block">
                    <div class="main-banner__title">{{ __('app.welcome.title') }}</div>
                    <div class="main-banner__name">Zamzam Medical Centre</div>
                    <div class="main-banner__hint">{{ __('app.welcome.feedback_title') }}</div>
                    <form class="main-banner__form" action="{{ route('feedback') }}" method="post" id="feedbackForm">
                        {{ csrf_field() }}
                        <input class="main-banner__input" name="phone" placeholder="{{ __('app.welcome.feedback_mask') }}"/>
                        <button class="g-recaptcha"
                                data-sitekey="6LfHMlcaAAAAAG7t04y5o30DYh98CbSKhpknpbUn"
                                data-callback='feedbackForm'
                                data-action='submit'>{{ __('app.welcome.feedback_button') }}</button>
                    </form>
                    <div class="main-banner-video">
                        <img src="/images/content/play-video.svg" alt=""/>
                        <div class="main-banner-video__text">
                            <div class="main-banner-video__hint">{{ __('app.layout.footer.title') }}</div>
                            <a href="#">{{ __('app.welcome.feedback_video') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <form class="mobile-feedback-form d-md-none d-lg-none d-xl-none" action="{{ route('feedback') }}" method="post" id="mobileFeedbackForm">
            {{ csrf_field() }}
            <div class="mobile-feedback-form__title">{{ __('app.welcome.feedback_title') }}</div>
            <input class="mobile-feedback-form" name="phone" placeholder="{{ __('app.welcome.feedback_mask') }}"/>
            <button class="g-recaptcha"
                    data-sitekey="6LfHMlcaAAAAAG7t04y5o30DYh98CbSKhpknpbUn"
                    data-callback='mobileFeedbackForm'
                    data-action='submit'>{{ __('app.welcome.feedback_button') }}</button>
        </form>
    </section>

    <section class="second-block">
        <div class="second-block__background">
            <div class="second-block__title">{{ __('app.welcome.about.title') }}</div>
            <div class="second-block__text">{!! __('app.welcome.about.text') !!}</div>
            <div class="second-block__steps">
                <div class="second-block__step">
                    <img src="/images/content/indication.svg"/>
                    <div class="second-block-step__title">{{ __('app.welcome.about.steps.program.title') }}</div>
                    <div class="second-block-step__text">{{ __('app.welcome.about.steps.program.text') }}</div>
                </div>
                <div class="second-block__step">
                    <img src="/images/content/indication.svg"/>
                    <div class="second-block-step__title">{{ __('app.welcome.about.steps.principe.title') }}</div>
                    <div class="second-block-step__text">{{ __('app.welcome.about.steps.principe.text') }}</div>
                </div>
                <div class="second-block__step">
                    <img src="/images/content/indication.svg"/>
                    <div class="second-block-step__title">{{ __('app.welcome.about.steps.standard.title') }}</div>
                    <div class="second-block-step__text">{{ __('app.welcome.about.steps.standard.text') }}</div>
                </div>
                <div class="second-block__step">
                    <img src="/images/content/circle.svg"/>
                    <div class="second-block-step__title">{{ __('app.welcome.about.steps.target.title') }}</div>
                    <div class="second-block-step__text">{{ __('app.welcome.about.steps.target.text') }}</div>
                </div>
            </div>
        </div>
    </section>

    <section class="quotes-block">
        <div class="swiper-container">
            <!-- Additional required wrapper -->
            <div class="swiper-wrapper">
                <!-- Slides -->
                <div class="swiper-slide">
                    <div class="quotes-block__content">
                        <div class="quotes-block__text">
                            <p>"Наша миссия - это улучшение качества жизни каждого пациента с применением новейших медицинских методик международного уровня.</p>

                            <p>Мы гордимся выбором наших пациентов и хотим оставаться надежным медицинским партнером с многолетней историей для целых поколений семей в Казахстане,
                                из года в год доверяющих нам своё здоровье и здоровье своих близких."</p>
                        </div>
                        <div class="quotes-block__author">Ахмерова Зауре Айдаровна</div>
                        <div class="quotes-block__about">Директор филиальной сети по Республике Казахстан</div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="quotes-block__content">
                        <div class="quotes-block__text">
                            <p>"Наша миссия - это улучшение качества жизни каждого пациента с применением новейших медицинских методик международного уровня.</p>

                            <p>Мы гордимся выбором наших пациентов и хотим оставаться надежным медицинским партнером с многолетней историей для целых поколений семей в Казахстане,
                                из года в год доверяющих нам своё здоровье и здоровье своих близких."</p>
                        </div>
                        <div class="quotes-block__author">Ахмерова Зауре Айдаровна</div>
                        <div class="quotes-block__about">Директор филиальной сети по Республике Казахстан</div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="quotes-block__content">
                        <div class="quotes-block__text">
                            <p>"Наша миссия - это улучшение качества жизни каждого пациента с применением новейших медицинских методик международного уровня.</p>

                            <p>Мы гордимся выбором наших пациентов и хотим оставаться надежным медицинским партнером с многолетней историей для целых поколений семей в Казахстане,
                                из года в год доверяющих нам своё здоровье и здоровье своих близких."</p>
                        </div>
                        <div class="quotes-block__author">Ахмерова Зауре Айдаровна</div>
                        <div class="quotes-block__about">Директор филиальной сети по Республике Казахстан</div>
                    </div>
                </div>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </section>

    <section class="services-block">
        <div class="services-block__title">{{ __('app.welcome.services.title') }}</div>
        <div class="services-block__subtitle">{{ __('app.welcome.services.text') }}</div>
        <div class="services-block__steps">
            <div class="service-step">
                <div class="service-step__number">1</div>
                <div class="service-step__title">{{ __('app.welcome.services.direction_title') }}</div>
                <div class="service-step__text">{{ __('app.welcome.services.direction_text') }}</div>
            </div>
            <div class="service-step">
                <div class="service-step__number service-step__number--highlight">2</div>
                <div class="service-step__title service-step__title--highlight">{{ __('app.welcome.services.estate_title') }}</div>
                <div class="service-step__text">{{ __('app.welcome.services.estate_text') }}</div>
            </div>
            <div class="service-step">
                <div class="service-step__number">3</div>
                <div class="service-step__title">{{ __('app.welcome.services.diagnostic_title') }}</div>
                <div class="service-step__text">{{ __('app.welcome.services.diagnostic_text') }}</div>
            </div>
        </div>

        <div class="service-components">
            <div class="service-component">
                <div class="service-component__name">Амбулаторная хирургия</div>
                <div class="service-component__list">
                    <div class="service-component__column">
                        <div class="service-component__item">Обрезание крайней плоти</div>
                        <div class="service-component__item">Карбункул</div>
                        <div class="service-component__item">Вросший ноготь</div>
                        <div class="service-component__item">Атерома</div>
                    </div>
                    <div class="service-component__column">
                        <div class="service-component__item">Липома</div>
                        <div class="service-component__item">Липома</div>
                        <div class="service-component__item">Фурункул</div>
                        <div class="service-component__item">Панариций</div>
                    </div>
                    <div class="service-component__column">
                        <div class="service-component__item">Липома</div>
                        <div class="service-component__item">Липома</div>
                        <div class="service-component__item">Фурункул</div>
                        <div class="service-component__item">Панариций</div>
                    </div>
                </div>
            </div>
            <div class="service-component">
                <div class="service-component__name">Амбулаторная хирургия</div>
                <div class="service-component__list">
                    <div class="service-component__column">
                        <div class="service-component__item">Обрезание крайней плоти</div>
                        <div class="service-component__item">Карбункул</div>
                        <div class="service-component__item">Вросший ноготь</div>
                        <div class="service-component__item">Атерома</div>
                    </div>
                    <div class="service-component__column">
                        <div class="service-component__item">Липома</div>
                        <div class="service-component__item">Липома</div>
                        <div class="service-component__item">Фурункул</div>
                        <div class="service-component__item">Панариций</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="service-components">
            <div class="service-component">
                <div class="service-component__name">Ведение беременности и Роды</div>
                <div class="service-component__list">
                    <div class="service-component__column">
                        <div class="service-component__item">Роды в родильном доме Рахат</div>
                        <div class="service-component__item">Программа по ведению беременности «Я стану мамой»</div>
                        <div class="service-component__item">Программа Психологической подготовки к родам</div>
                        <div class="service-component__item">Программа Психологической подготовки к родам</div>
                    </div>
                </div>
            </div>
            <div class="service-component">
                <div class="service-component__name">Гастроэнтерология</div>
                <div class="service-component__list">
                    <div class="service-component__column">
                        <div class="service-component__item">Киста печени</div>
                        <div class="service-component__item">ГЭРБ</div>
                        <div class="service-component__item">Синдром Жильбера</div>
                        <div class="service-component__item">Дистрофия</div>
                    </div>
                    <div class="service-component__column">
                        <div class="service-component__item">Хеликобактер пилори</div>
                        <div class="service-component__item">Запор</div>
                        <div class="service-component__item">Запор</div>
                        <div class="service-component__item">Синдром раздраженного кишечника</div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="our-specs">
        <div class="our-specs__title">{{ __('app.welcome.our_specs.title') }}</div>
        <div class="our-specs__subtitle">{{ __('app.welcome.our_specs.text') }}</div>
        <div class="our-specs__list">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="our-specs-item">
                            <img src="/images/content/spec-1.png" alt=""/>
                            <div class="our-specs-item__name">Гопкало Эдуард Борисович</div>
                            <div class="our-specs-item__about">Врач уролог-андролог, сексолог</div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="our-specs-item">
                            <img src="/images/content/spec-2.png" alt=""/>
                            <div class="our-specs-item__name">Гопкало Эдуард Борисович</div>
                            <div class="our-specs-item__about">Врач уролог-андролог, сексолог</div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="our-specs-item">
                            <img src="/images/content/spec-3.png" alt=""/>
                            <div class="our-specs-item__name">Гопкало Эдуард Борисович</div>
                            <div class="our-specs-item__about">Врач уролог-андролог, сексолог</div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="our-specs-item">
                            <img src="/images/content/spec-4.png" alt=""/>
                            <div class="our-specs-item__name">Гопкало Эдуард Борисович</div>
                            <div class="our-specs-item__about">Врач уролог-андролог, сексолог</div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="our-specs-item">
                            <img src="/images/content/spec-1.png" alt=""/>
                            <div class="our-specs-item__name">Гопкало Эдуард Борисович</div>
                            <div class="our-specs-item__about">Врач уролог-андролог, сексолог</div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="our-specs-item">
                            <img src="/images/content/spec-2.png" alt=""/>
                            <div class="our-specs-item__name">Гопкало Эдуард Борисович</div>
                            <div class="our-specs-item__about">Врач уролог-андролог, сексолог</div>
                        </div>
                    </div>
                </div>

                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
            </div>
        </div>
    </section>

    <section class="promo-block">
        <div class="promo-block__title">{{ __('app.layout.menu.actions') }}</div>
        <div class="promo-block__subtitle">{{ __('app.welcome.our_specs.text') }}</div>
        <div class="promo-block__list  d-none d-md-flex">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    @foreach($actions as $action)
                        <a class="swiper-slide" href="{{ route('action', ['action' => $action->id]) }}">
                            <img src="{{ $action->preview }}" alt=""/>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="promo-block__list d-md-none d-lg-none d-xl-none">
            <a class="swiper-slide" href="#">
                <img src="/images/content/promo-1.png" alt=""/>
            </a>
            <a class="swiper-slide" href="#">
                <img src="/images/content/promo-2.png" alt=""/>
            </a>
            <a class="swiper-slide" href="#">
                <img src="/images/content/promo-3.png" alt=""/>
            </a>
        </div>
    </section>

    @include('_contacts')

    <div class="record-float-btn">
        {{ __('app.welcome.record_button') }}
    </div>

    <div class="modal-component record-modal">
        <div class="modal-component__wrapper">
            @include('_modal-report')
        </div>
    </div>
@endsection
