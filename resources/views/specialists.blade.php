@extends('_layout')

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('main') }}">{{ __('app.layout.menu.main') }}</a></li>
            <li class="breadcrumb-item active">{{ __('app.layout.menu.specialists') }}</li>
        </ol>
    </nav>

    <div class="specialists-page">
        <div class="specialists-page__title">{{ __('app.layout.menu.specialists') }}</div>

        <form class="specialists-filter">
            <select class="specialists-dropdown specialists-dropdown--cities">
                <option name="almaty">Алматы</option>
            </select>

            <select class="specialists-dropdown specialists-dropdown--clinics">
                <option name="">Клиника</option>
            </select>

            <select class="specialists-dropdown specialists-dropdown--specialization" name="specialization_id">
                <option value="">{{ __('app.layout.menu.specialists') }}</option>
                @foreach($specializations as $specialization)
                    <option value="{{ $specialization->id }}" {{ $search['specialization'] === $specialization->id ? 'selected' : '' }}>
                        {{ $specialization->name }}
                    </option>
                @endforeach
            </select>

            <div class="specialists-search-input">
                <img src="/images/content/zoom-in.svg" alt="" class="specialists-search-input__icon"/>
                <input class="specialists-input" placeholder="{{ __('app.specialists.input_search') }}"/>
            </div>

            <button class="specialists-button">{{ __('app.specialists.button_search') }}</button>
        </form>

        @foreach($specializations as $specialization)
            @if (empty($specialization['doctors']))
                @continue
            @endif
            <div class="specialists-list">
                <div class="specialists-list__title">{{ $specialization->name }}</div>
                <div class="specialists-list__wrapper">
                    @foreach($specialization['doctors'] as $doctor)
                        <div class="our-specs-item">
                            <img src="{{ $doctor->image }}" alt=""/>
                            <div class="our-specs-item__name">{{ $doctor->name }}</div>
                            <div class="our-specs-item__about">{{ $doctor->title }}</div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endforeach
    </div>

@endsection
