@extends('_layout')

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('main') }}">{{ __('app.layout.menu.main') }}</a></li>
            <li class="breadcrumb-item active">Логин</li>
        </ol>
    </nav>

    <section class="services-block" style="max-width: 400px">
        <div class="services-block__title">Login</div>
        <div class="services-block__subtitle">{{ __('app.welcome.services.text') }}</div>
        <form method="post" action="{{ route('auth') }}">
            <input class="form-control" name="key" type="password"/>
            <button class="form-control btn-outline-primary">Login</button>
            {{ csrf_field() }}
        </form>
    </section>
@endsection
