<section class="contacts-block">
    <div class="contacts-block__title">{{ __('app.layout.menu.contacts') }}</div>
    <div class="contacts-block__subtitle">{{ __('app.welcome.our_specs.text') }}</div>
    <div class="contacts-block__list">
        <div class="contacts-block-item">
            <div class="contacts-block-item__header">
                <div class="contacts-block-item__address">г. Алматы</div>
                <div class="contacts-block-item__time">08:00 - 21:00</div>
            </div>
            <div class="contacts-block-item__bold">
                <div class="contacts-block-item__address">Мендикулова бульвар, 105</div>
                <div class="contacts-block-item__time">Пн - Вс</div>
            </div>
            <div class="contacts-block-item__header">Самал 2-й микрорайон, жилой комплекс Фантазия</div>
            <div class="contacts-block-item__phones">
                <div class="contacts-block-item__phone">
                    <img src="/images/content/phone.svg" alt=""/>
                    <span>+7 727 272-33-43</span>
                </div>
                <div class="contacts-block-item__phone">
                    <img src="/images/content/whatsapp-mini.svg" alt=""/>
                    <span>+7 727 272-33-43</span>
                </div>
                <div class="contacts-block-item__phone">
                    <img src="/images/content/telegram-mini.svg" alt=""/>
                    <span>+7 727 272-33-43</span>
                </div>
            </div>
        </div>
        <div class="contacts-block-item">
            <div class="contacts-block-item__header">
                <div class="contacts-block-item__address">г. Алматы</div>
                <div class="contacts-block-item__time">08:00 - 21:00</div>
            </div>
            <div class="contacts-block-item__bold">
                <div class="contacts-block-item__address">Мендикулова бульвар, 105</div>
                <div class="contacts-block-item__time">Пн - Вс</div>
            </div>
            <div class="contacts-block-item__header">Самал 2-й микрорайон, жилой комплекс Фантазия</div>
            <div class="contacts-block-item__phones">
                <div class="contacts-block-item__phone">
                    <img src="/images/content/phone.svg" alt=""/>
                    <span>+7 727 272-33-43</span>
                </div>
                <div class="contacts-block-item__phone">
                    <img src="/images/content/whatsapp-mini.svg" alt=""/>
                    <span>+7 727 272-33-43</span>
                </div>
                <div class="contacts-block-item__phone">
                    <img src="/images/content/telegram-mini.svg" alt=""/>
                    <span>+7 727 272-33-43</span>
                </div>
            </div>
        </div>
        <div class="contacts-block-item">
            <div class="contacts-block-item__header">
                <div class="contacts-block-item__address">г. Алматы</div>
                <div class="contacts-block-item__time">08:00 - 21:00</div>
            </div>
            <div class="contacts-block-item__bold">
                <div class="contacts-block-item__address">Мендикулова бульвар, 105</div>
                <div class="contacts-block-item__time">Пн - Вс</div>
            </div>
            <div class="contacts-block-item__header">Самал 2-й микрорайон, жилой комплекс Фантазия</div>
            <div class="contacts-block-item__phones">
                <div class="contacts-block-item__phone">
                    <img src="/images/content/phone.svg" alt=""/>
                    <span>+7 727 272-33-43</span>
                </div>
                <div class="contacts-block-item__phone">
                    <img src="/images/content/whatsapp-mini.svg" alt=""/>
                    <span>+7 727 272-33-43</span>
                </div>
                <div class="contacts-block-item__phone">
                    <img src="/images/content/telegram-mini.svg" alt=""/>
                    <span>+7 727 272-33-43</span>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="map-block">
    <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A71ffe894566d180fb7bb3c39a9adad12f1a77acd8df78c57fe9d8772184d55df&amp;width=100%25&amp;height=300&amp;lang=ru_RU&amp;scroll=true"></script>
</section>
