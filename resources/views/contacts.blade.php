@extends('_layout')

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('main') }}">{{ __('app.layout.menu.main') }}</a></li>
            <li class="breadcrumb-item active">{{ __('app.layout.menu.contacts') }}</li>
        </ol>
    </nav>

    @include('_contacts')
@endsection
