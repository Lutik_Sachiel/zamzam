@extends('_layout')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('main') }}">{{ __('app.layout.menu.main') }}</a></li>
            <li class="breadcrumb-item"><a href="{{ route('actions') }}">{{ __('app.layout.menu.actions') }}</a></li>
            <li class="breadcrumb-item active">{{ $action->name }}</li>
        </ol>
    </nav>

    <div class="actions-page">
        <div class="actions-page__title">{{ $action->name }}</div>
        <img src="{{ $action->cover }}" alt="" class="mb-3"/>
        <div class="">
            <div class="actions-content">
                <div class="actions-content__title">{{ $action->name }}</div>
                <div class="actions-content__text">{!! $action->text !!}</div>
            </div>
        </div>
    </div>

    <div class="report-form">
        <div class="report-form__wrapper">
            @include('_modal-report')
        </div>
    </div>
    @include('_contacts')
@endsection
