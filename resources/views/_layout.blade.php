<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Zamzam</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/css/app.css" rel="stylesheet">
    <script>
        window.lang = "{{ session('locale') }}";
    </script>
</head>
<body>
<header>
    <div class="header-top">
        <div class="header-top__slogan">{{ __('app.layout.title') }}</div>
        @if (session('locale') === 'ru')
            <div class="header-top__lang"><a href="{{ route('setLocale', ['locale' => 'kk']) }}">KK</a></div>
        @else
            <div class="header-top__lang"><a href="{{ route('setLocale', ['locale' => 'ru']) }}">RU</a></div>
        @endif
    </div>

    <div class="header-menu">
        <div class="menu-btn d-md-none d-lg-none d-xl-none"><img src="/images/content/menu.svg" alt=""/></div>
        <div class="header-menu__left">
            <div class="header-menu__logo">
                <a href="{{ route('main') }}"><img src="/images/content/logo.jpg" alt="logo"/></a>
            </div>
            <div class="header-menu__items">
                <div class="header-menu__item">
                    {{ __('app.layout.menu.about_us') }}
                </div>
                <div class="header-menu__item">
                    <a href="{{ route('services') }}">{{ __('app.layout.menu.services') }}</a>
                </div>
                <div class="header-menu__item">
                    <a href="{{ route('specialists') }}">{{ __('app.layout.menu.specialists') }}</a>
                </div>
                <div class="header-menu__item">
                    <a href="{{ route('actions') }}">{{ __('app.layout.menu.actions') }}</a>
                </div>
                <div class="header-menu__item">
                    <a href="{{ route('contacts') }}">{{ __('app.layout.menu.contacts') }}</a>
                </div>
            </div>
        </div>

        <div class="header-menu__right">
            <a href="#"><img src="/images/content/instagram.svg" alt="instagram"/></a>
            <a href="#"><img src="/images/content/whatsapp.svg" alt="whatsapp"/></a>
            <a href="#"><img src="/images/content/telegram.svg" alt="telegram"/></a>
            <a href="#"><img src="/images/content/youtube.svg" alt="youtube"/></a>
        </div>
    </div>

    <div class="header-menu-items d-none">
        <div class="header-menu__item">
            {{ __('app.layout.menu.about_us') }}
        </div>
        <div class="header-menu__item">
            {{ __('app.layout.menu.services') }}
        </div>
        <div class="header-menu__item">
            <a href="{{ route('specialists') }}">{{ __('app.layout.menu.specialists') }}</a>
        </div>
        <div class="header-menu__item">
            <a href="{{ route('actions') }}">{{ __('app.layout.menu.actions') }}</a>
        </div>
        <div class="header-menu__item">
            <a href="{{ route('contacts') }}">{{ __('app.layout.menu.contacts') }}</a>
        </div>
    </div>
</header>

@yield('content')

<footer>
    <div class="d-none d-md-flex">
        <div class="footer-column footer-column--first">
            <a href="{{ route('main') }}"><img src="/images/content/logo.jpg" alt=""/></a>
            <div class="footer-column__about">
                {{ __('app.layout.footer.title') }}
            </div>
            <div class="footer-column__sites">
                <a href="#"><img src="/images/content/instagram.svg" alt=""/></a>
                <a href="#"><img src="/images/content/whatsapp.svg" alt=""/></a>
                <a href="#"><img src="/images/content/telegram.svg" alt=""/></a>
                <a href="#"><img src="/images/content/youtube.svg" alt=""/></a>
            </div>
        </div>
        <div class="footer-column">
            <div class="footer-column__title">{{ __('app.layout.footer.about_us') }}</div>

            <div class="footer-column__link">{{ __('app.layout.menu.about_us') }}</div>
            <div class="footer-column__link">{{ __('app.layout.menu.services') }}</div>
            <div class="footer-column__link"><a href="{{ route('specialists') }}">{{ __('app.layout.menu.specialists') }}</a></div>
            <div class="footer-column__link"><a href="{{ route('actions') }}">{{ __('app.layout.menu.actions') }}</a></div>
            <div class="footer-column__link"><a href="{{ route('contacts') }}">{{ __('app.layout.menu.contacts') }}</a></div>
        </div>
        <div class="footer-column">
            <div class="footer-column__title">{{ __('app.layout.footer.pop_services') }}</div>

            <div class="footer-column__link">Карбункул</div>
            <div class="footer-column__link">Роды в родильном доме Рахат</div>
            <div class="footer-column__link">Хеликобактер пилори</div>
            <div class="footer-column__link">Вросший ноготь</div>
        </div>
        <div class="footer-column footer-column--offset">
            <div class="footer-column__link">Карбункул</div>
            <div class="footer-column__link">Роды в родильном доме Рахат</div>
            <div class="footer-column__link">Хеликобактер пилори</div>
            <div class="footer-column__link">Вросший ноготь</div>
        </div>
        <div class="footer-column footer-column--offset">
            <div class="footer-column__link">Карбункул</div>
            <div class="footer-column__link">Роды в родильном доме Рахат</div>
            <div class="footer-column__link">Хеликобактер пилори</div>
            <div class="footer-column__link">Вросший ноготь</div>
        </div>
        <div class="footer-column">
            <div class="footer-column__title">{{ __('app.layout.menu.contacts') }}</div>

            <div class="footer-column__link">+7 727 3999404</div>
            <div class="footer-column__link">+7 727 3999404</div>
            <div class="footer-column__link">+7 727 3999404</div>
            <div class="footer-column__address">г.Алматы, Мендикулова бульвар, 105</div>
            <div class="footer-column__address">Info@zamzam-med.kz</div>
        </div>
    </div>
    <div class="d-md-none d-lg-none d-xl-none d-flex">
        <div class="footer-column">
            <div class="footer-column__title">{{ __('app.layout.footer.about_us') }}</div>

            <div class="footer-column__link">{{ __('app.layout.menu.about_us') }}</div>
            <div class="footer-column__link">{{ __('app.layout.menu.services') }}</div>
            <div class="footer-column__link"><a href="{{ route('specialists') }}">{{ __('app.layout.menu.specialists') }}</a></div>
            <div class="footer-column__link"><a href="{{ route('actions') }}">{{ __('app.layout.menu.actions') }}</a></div>
            <div class="footer-column__link"><a href="{{ route('contacts') }}">{{ __('app.layout.menu.contacts') }}</a></div>
        </div>
        <div class="footer-column">
            <div class="footer-column__title">{{ __('app.layout.menu.contacts') }}</div>

            <div class="footer-column__link">+7 727 3999404</div>
            <div class="footer-column__link">+7 727 3999404</div>
            <div class="footer-column__link">+7 727 3999404</div>
            <div class="footer-column__address">г.Алматы, Мендикулова бульвар, 105</div>
            <div class="footer-column__address">Info@zamzam-med.kz</div>
        </div>
    </div>
    <div class="d-md-none d-lg-none d-xl-none d-flex flex-column align-items-baseline">
        <a href="{{ route('main') }}"><img src="/images/content/logo.jpg" alt=""/></a>
        <div class="footer-column__about">
            {{ __('app.layout.footer.title') }}
        </div>
        <div class="footer-column__sites">
            <a href="#"><img src="/images/content/instagram.svg" alt=""/></a>
            <a href="#"><img src="/images/content/whatsapp.svg" alt=""/></a>
            <a href="#"><img src="/images/content/telegram.svg" alt=""/></a>
            <a href="#"><img src="/images/content/youtube.svg" alt=""/></a>
        </div>
    </div>
</footer>

<section class="copyright-block">
    <div>© Copyright 2016 - 2020 Zamzam. All Rights Reserved.</div>
</section>

<script src="https://www.google.com/recaptcha/api.js"></script>
<script type="application/javascript" src="/js/app.js"></script>
</body>
</html>
