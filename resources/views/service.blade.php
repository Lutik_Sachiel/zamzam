@extends('_layout')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('main') }}">{{ __('app.layout.menu.main') }}</a></li>
            <li class="breadcrumb-item"><a href="{{ route('actions') }}">{{ __('app.layout.menu.services') }}</a></li>
            <li class="breadcrumb-item active">{{ $service->title }}</li>
        </ol>
    </nav>

    <div class="actions-page">
        <div class="actions-page__title">{{ __('app.layout.menu.services') }}</div>
        <div class="">
            <div class="actions-content">
                <div class="actions-content__title">{{ $service->title }}</div>
                <div class="actions-content__text">{!! $service->shortcut !!}</div>
                <div class="actions-content__tabs">
                    <a class="actions-content__tab @if ($mode === 'description') actions-content__tab--selected @endif" href="{{ route('service', ['service' => $service, 'mode' => 'description']) }}">Описание</a>
                    <a class="actions-content__tab @if ($mode === 'doctors') actions-content__tab--selected @endif" href="{{ route('service', ['service' => $service, 'mode' => 'doctors']) }}">Врачи</a>
                </div>

                @if ($mode === 'description')
                    <div class="actions-content__text mt-2">{!! $service->description !!}</div>
                @elseif ($mode === 'doctors')
                    <div class="specialists-list mt-2">
                        <div class="specialists-list__wrapper">
                            @foreach($service->doctors as $doctor)
                                <div class="our-specs-item">
                                    <img src="{{ $doctor['image'] }}" alt=""/>
                                    <div class="our-specs-item__name">{{ $doctor['name'] }}</div>
                                    <div class="our-specs-item__about">{{ $doctor['title'] }}</div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
