import Swiper from 'swiper/bundle';
import 'swiper/swiper-bundle.min.css'

const swiper = new Swiper('.quotes-block .swiper-container', {
    loop: true,

    pagination: {
        el: '.swiper-pagination',
        clickable: true
    },
});

new Swiper('.our-specs .swiper-container', {
    loop: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    slidesPerView: 4,
    initialSlide: 0,
    spaceBetween: 45,
    breakpoints: {
        320: {
            slidesPerView: 2,
            spaceBetween: 10,
        },
        600: {
            slidesPerView: 4,
            spaceBetween: 45,
        }
    }
});

new Swiper('.promo-block .swiper-container', {
    loop: true,
    slidesPerView: 4,
    centeredSlides: true,
    initialSlide: 1,
    spaceBetween: 15,
});

const menuBtn = document.querySelector('.menu-btn');
const menuItems = document.querySelector('.header-menu-items')

menuBtn.addEventListener('click', function () {
    menuItems.classList.toggle('d-none')
})

const toggleModal = function (event) {
    if (event.target === event.currentTarget) {
        document.querySelector('.record-modal').classList.toggle('modal-component--active')
        document.querySelector('body').classList.toggle('overflow-hidden');
    }
};

let recordBtn = document.querySelector('.record-float-btn');
if (recordBtn) {
    recordBtn.addEventListener('click', toggleModal);
}

let recordModal = document.querySelector('.record-modal');
if (recordModal) {
    recordModal.addEventListener('click', toggleModal);
}

function requestForm(token) {
    document.getElementById("request-form").submit();
}

function feedbackForm(token) {
    document.getElementById("feedbackForm").submit();
}

function mobileFeedbackForm(token) {
    document.getElementById("mobileFeedbackForm").submit();
}

document
    .querySelectorAll('.js-show-specialization')
    .forEach(el => el.addEventListener('click', event => {
        fetch('/ajax/services/'+event.target.dataset.specialization).then(async response => {
            const json = await response.json();
            const lang = window.lang;
            let services = '';

            json.forEach(service => {
                services += '<a href="/services/'+service.id+'">'+service['name_'+lang]+'</a>'
            })
            document.getElementById('services-specialization-'+event.target.dataset.specialization).innerHTML = services
        })

        event.target.classList.add('d-none');
        document.getElementById('hide-specialization-'+event.target.dataset.specialization).classList.remove('d-none');
        document.getElementById('info-specialization-'+event.target.dataset.specialization).classList.remove('d-none');
    }));

document
    .querySelectorAll('.js-hide-specialization')
    .forEach(el => el.addEventListener('click', event => {
        event.target.classList.add('d-none')
        document.getElementById('info-specialization-'+event.target.dataset.specialization).classList.add('d-none');
        document.getElementById('show-specialization-'+event.target.dataset.specialization).classList.remove('d-none');
    }))
